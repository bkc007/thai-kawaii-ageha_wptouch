<?php get_header(); ?>

	<div id="content">
		<?php while ( wptouch_have_posts() ) { ?>

			<?php wptouch_the_post(); ?>

			<div class="<?php wptouch_post_classes(); ?>">
				<div class="post-page-head-area bauhaus">

					<h1 class="post-title heading-font"><?php wptouch_the_title(); ?></h1>
					<?php if ( bauhaus_should_show_author() ) { ?>
						<span class="post-author"><?php the_author(); ?></span>
					<?php } ?>
					<?php if ( bauhaus_should_show_thumbnail() && wptouch_has_post_thumbnail() ) { ?>
						<div class="post-page-thumbnail">
							<?php the_post_thumbnail('large', array( 'class' => 'post-thumbnail wp-post-image' ) ); ?>
						</div>
					<?php } ?>
				</div>

				<div class="post-page-content">
					<?php wptouch_the_content(); ?>

<?php if ( function_exists( "get_yuzo_related_posts" ) ) { get_yuzo_related_posts(); } ?>

					<?php if ( bauhaus_should_show_taxonomy() ) { ?>
						<?php if ( wptouch_has_categories() || wptouch_has_tags() ) { ?>
							<div class="cat-tags">
								<?php if ( wptouch_has_categories() ) { ?>
									<?php _e( 'Categories', 'wptouch-pro' ); ?>: <?php wptouch_the_categories(); ?><br />
								<?php } ?>
								<?php if ( wptouch_has_tags() ) { ?>
									<?php _e( 'Tags', 'wptouch-pro' ); ?>: <?php wptouch_the_tags(); ?>
								<?php } ?>
							</div>
						<?php } ?>
					<?php } ?>
				</div>
			</div>

		<?php } ?>
	</div> <!-- content -->

	<?php do_action( 'wptouch_after_post_content' ); ?>

	<?php get_template_part( 'nav-bar' ); ?>


<hr style="clear:both !important;" / >


	<div style="padding-left:2% !important; padding-top : 10px !important;">

	<h3 class="related_post_title" style="padding-left:5px !important;">アゲハグループ連絡先</h3>


	<p style=" text-align:center !important;">

	<ul class="related_post wp_rp" style="text-align:center !important;">

<li data-position="0" data-poid="in-10470" data-post-type="none"  >
	<a href="http://www.thai-kawaii-ageha.com/"><img src="http://www.thai-kawaii-ageha.com/blog/wp-content/uploads/2018/03/ageha_main_banner.jpg"></a>
</li>

<li data-position="0" data-poid="in-10470" data-post-type="none"  >
	<a href="http://www.thai-kawaii-ageha.com/blog/"><img src="http://www.thai-kawaii-ageha.com/wp-content/uploads/2017/01/blog_banner.jpg" ></a>
</li>

<li data-position="0" data-poid="in-10470" data-post-type="none"  >
	<a href="http://www.thai-kawaii-ageha.com/prince"><img src="http://www.thai-kawaii-ageha.com/wp-content/uploads/2017/01/prince_banner.jpg" ></a>
</li>

<li data-position="0" data-poid="in-10470" data-post-type="none"  >
	<a href="http://www.thai-kawaii-ageha.com/girls/"><img src="http://www.thai-kawaii-ageha.com/wp-content/uploads/2017/01/girls_banner.jpg"></a>
</li>

<li data-position="0" data-poid="in-10470" data-post-type="none"  >
	<a href="https://agechin.xyz/">
		<img src="https://www.thai-kawaii-ageha.com/wp-content/uploads/2019/12/agechin_white.jpg" alt="あげちん" />
	</a>
</li>

<li data-position="0" data-poid="in-10470" data-post-type="none"  >
	<a href="https://bangkok-marilynmonroe.com/">
		<img src="https://www.thai-kawaii-ageha.com/wp-content/uploads/2019/12/marilynmonroe.jpg" alt="マリリン・モンロー" />
	</a>
</li>

<li data-position="0" data-poid="in-10470" data-post-type="none"  >
	<a href="tel:026523007" class="wp_rp_thumbnail" >
		<img src="https://www.thai-kawaii-ageha.com/blog/wp-content/uploads/2018/03/ageha_shop_tel-1.jpg" alt="↑AGEHA店舗 連絡先 電話番号 026-523-007"  />
	</a>
</li>

<li data-position="0" data-poid="in-10470" data-post-type="none"  >
	<a href="tel:026523007" class="wp_rp_thumbnail" >
		<img src="https://www.thai-kawaii-ageha.com/wp-content/uploads/2019/12/agechin-tel.jpg" alt="↑あげちん店舗 連絡先 電話番号 026-523-007"  />
	</a>
</li>

<li data-position="0" data-poid="in-10470" data-post-type="none"  >
	<a href="tel:022666111" class="wp_rp_thumbnail" >
		<img src="https://www.thai-kawaii-ageha.com/blog/wp-content/uploads/2018/03/marilyn_tel.jpg" alt="↑マリリン店舗 連絡先 電話番号 022-666-111"  />
	</a>
</li>

<li data-position="0" data-poid="in-10470" data-post-type="none"  >
	<a href="https://www.facebook.com/ageha.bangkok/">
		<img src="https://www.thai-kawaii-ageha.com/wp-content/uploads/2017/01/facebook_banner.jpg" >
	</a>
</li>

<li data-position="0" data-poid="in-10470" data-post-type="none"  >
	<a href="https://www.facebook.com/agechin.bangkok/">
		<img src="https://www.thai-kawaii-ageha.com/wp-content/uploads/2019/12/facebook_agechin.jpg" >
	</a>
</li>

<li data-position="0" data-poid="in-10470" data-post-type="none" >
	<a href="tel:022337775" class="wp_rp_thumbnail" >
		<img src="https://www.thai-kawaii-ageha.com/blog/wp-content/uploads/2018/03/monroe_tel.jpg" alt="↑モンロー店舗 連絡先 電話番号 022-33-7775"  />
	</a>
</li>

<li data-position="0" data-poid="in-10470" data-post-type="none"  >
	<a href="https://twitter.com/agehathai?lang=ja">
		<img src="https://www.thai-kawaii-ageha.com/wp-content/uploads/2017/01/twitter_banner.jpg" >
	</a>
</li>

<li data-position="0" data-poid="in-10470" data-post-type="none"  >
	<a href="tel:+66820873111" class="wp_rp_thumbnail" >
		<img src="https://www.thai-kawaii-ageha.com/blog/wp-content/uploads/2018/03/abu_phone-1.jpg" alt="↑「タイ王国あげは店長あぶブログ」（事前電話予約はコチラ)"  />
	</a>
</li>

<li data-position="0" data-poid="in-10470" data-post-type="none"  >
	<a href="tel:+66655902655" class="wp_rp_thumbnail" >
		<img src="https://www.thai-kawaii-ageha.com/blog/wp-content/uploads/2018/03/shun_phone.jpg" alt="↑「バンコク王子の成長物語」（事前Eメール予約はコチラ)"  />
	</a>
</li>

<li data-position="0" data-poid="in-10470" data-post-type="none"  >
	<a href="tel:+66936156522" class="wp_rp_thumbnail" >
		<img src="https://www.thai-kawaii-ageha.com/wp-content/uploads/2019/12/toda_phone.jpg" alt="↑「マリリン＆モンロー＠パンチラタイフーン」（事前Eメール予約はコチラ）"  />
	</a>
</li>

<li data-position="0" data-poid="in-10470" data-post-type="none"  >
	<a href="tel:+66967712983" class="wp_rp_thumbnail" >
		<img src="https://www.thai-kawaii-ageha.com/wp-content/uploads/2019/12/toshi_phone.jpg" alt="↑「あげちん！出来る男たちの社交場」（事前Eメール予約はコチラ）"  />
	</a>
</li>

<li data-position="0" data-poid="in-10470" data-post-type="none"  >
	<a href="tel:+66644630348" class="wp_rp_thumbnail" >
		<img src="https://www.thai-kawaii-ageha.com/wp-content/uploads/2019/12/ryou_phone.jpg" alt="↑「日本人夜遊び案内ガイド」リョウ"  />
	</a>
</li>

	</ul>
		</p>

	</div>




</div>

<br />




<?php get_footer(); ?>
