<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<title><?php echo esc_html( wp_title( ' | ', false, 'right' ) ); ?></title>
		<?php wptouch_head(); ?>
		<?php
			if ( !is_single() && !is_archive() && !is_page() && !is_search() ) {
				wptouch_canonical_link();
			}

			if ( isset( $_REQUEST[ 'wptouch_preview_theme' ] ) || isset( $_REQUEST[ 'wptouch_switch' ] ) )  {
				echo '<meta name="robots" content="noindex" />';
			}
		?>

    <style>

		.post-page-content {

			padding-right : 30px !important;
			padding-right : 10px !important;

		}

		#wp_rp_first {

			margin-left : auto;
			margin-right: auto;

		}


		ul.related_post {

			width: 100% !important;

		}

		ul.related_post li {

			width : 32% !important;
		  margin: 0px 0px 0px 0px !important;

		}

		ul.related_post li img {

			width : 100% !important;

		}


		.page-wrapper {


		margin-bottom : 0px !important;


		}


		.wp_rp_thumbnail {
	    width: 145px !important;
		}




		ul.related_post li a {
		    line-height: 1.3em !important;
		    width: 100% !important;
		}



		ul.related_post li img {
		    width: 100% !important;
		}





		ul.related_post li img {
			display: block !important;
			width: 100% !important;
			height: auto !important;
			max-width: 100% !important;
			margin: 0 !important;
			padding: 0 !important;
			background: none !important;
			border: none !important;

			border-radius: 3px !important;
			box-shadow: 0 1px 4px rgba(0, 0, 0, 0.2) !important;
		}




		#myGallery, #myGallerySet, #flickrGallery {
		    height: 250px !important;
		    width: 100% !important;
		}



    </style>

	</head>

	<body <?php body_class( wptouch_get_body_classes() ); ?>>

		<?php do_action( 'wptouch_preview' ); ?>

		<?php do_action( 'wptouch_body_top' ); ?>

		<?php get_template_part( 'header-bottom' ); ?>

		<?php do_action( 'wptouch_body_top_second' ); ?>
