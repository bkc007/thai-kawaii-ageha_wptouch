<?php get_header(); ?>



	<div id="content">
		<?php while ( wptouch_have_posts() ) { ?>

			<?php wptouch_the_post(); ?>

			<div class="<?php wptouch_post_classes(); ?>">
				<div class="post-page-head-area bauhaus">
					<span class="post-date-comments">
						<?php if ( bauhaus_should_show_date() ) { ?>
							<?php wptouch_the_time(); ?>
						<?php } ?>
						<?php if ( bauhaus_should_show_comment_bubbles() ) { ?>
							<?php if ( bauhaus_should_show_date() && ( comments_open() || wptouch_have_comments() ) ) echo '&bull;'; ?>
							<?php if ( comments_open() || wptouch_have_comments() ) comments_number( __( 'no comments', 'wptouch-pro' ), __( '1 comment', 'wptouch-pro' ), __( '% comments', 'wptouch-pro' ) ); ?>
						<?php } ?>
					</span>
					<h2 class="post-title heading-font"><?php wptouch_the_title(); ?></h2>
					<?php if ( bauhaus_should_show_author() ) { ?>
						<span class="post-author"><?php the_author(); ?></span>
					<?php } ?>
				</div>

				<div class="post-page-content">
					<?php if ( bauhaus_should_show_thumbnail() && wptouch_has_post_thumbnail() ) { ?>
						<div class="post-page-thumbnail">
							<?php the_post_thumbnail('large', array( 'class' => 'post-thumbnail wp-post-image' ) ); ?>
						</div>
					<?php } ?>




					<?php wptouch_the_content(); ?>



					<div style="padding-left:2% !important;">

					<h3 class="related_post_title" style="padding-left:5px !important;">アゲハグループ連絡先</h3>


					<p style=" text-align:center !important;">

					<ul class="related_post wp_rp" style="text-align:center !important;">

						<li data-position="0" data-poid="in-10470" data-post-type="none"  >
							<a href="http://www.thai-kawaii-ageha.com/"><img src="http://www.thai-kawaii-ageha.com/blog/wp-content/uploads/2018/03/ageha_main_banner.jpg"></a>
						</li>


					<li data-position="0" data-poid="in-10470" data-post-type="none"  >
						<a href="http://www.thai-kawaii-ageha.com/blog/"><img src="http://www.thai-kawaii-ageha.com/wp-content/uploads/2017/01/blog_banner.jpg" ></a>
					</li>

					<li data-position="0" data-poid="in-10470" data-post-type="none"  >
						<a href="http://www.thai-kawaii-ageha.com/girls/"><img src="http://www.thai-kawaii-ageha.com/wp-content/uploads/2017/01/girls_banner.jpg"></a>
					</li>


					<li data-position="0" data-poid="in-10470" data-post-type="none"  >
						<a href="http://www.thai-kawaii-ageha.com/comment/"><img src="http://www.thai-kawaii-ageha.com/wp-content/uploads/2017/01/commnet_banner.jpg" ></a>
					</li>

					<li data-position="0" data-poid="in-10470" data-post-type="none"  >
						<a href="https://www.facebook.com/ageha.bangkok/"><img src="http://www.thai-kawaii-ageha.com/wp-content/uploads/2017/01/facebook_banner.jpg" ></a>
					</li>


					<li data-position="0" data-poid="in-10470" data-post-type="none"  >
						<a href="https://twitter.com/agehathai?lang=ja"><img src="http://www.thai-kawaii-ageha.com/wp-content/uploads/2017/01/twitter_banner.jpg" ></a>
					</li>

					<li data-position="0" data-poid="in-10470" data-post-type="none"  >
						<a href="tel:+66820873111" class="wp_rp_thumbnail" ><img src="http://www.thai-kawaii-ageha.com/blog/wp-content/uploads/2018/03/abu_phone-1.jpg" alt="↑「タイ王国あげは店長あぶブログ」（事前電話予約はコチラ)"  /></a>
					</li>

					<li data-position="0" data-poid="in-10470" data-post-type="none"  >
						<a href="tel:026523007" class="wp_rp_thumbnail" ><img src="http://www.thai-kawaii-ageha.com/blog/wp-content/uploads/2018/03/ageha_shop_tel-1.jpg" alt="↑AGEHA店舗 連絡先 電話番号 026-523-007"  /></a>

					</li>


					<li data-position="0" data-poid="in-10470" data-post-type="none"  >
						<a href="tel:022666111" class="wp_rp_thumbnail" >
						<img src="http://www.thai-kawaii-ageha.com/blog/wp-content/uploads/2018/03/marilyn_tel.jpg" alt="↑マリリン店舗 連絡先 電話番号 022-666-111"  /></a>

					</li>


					<li data-position="0" data-poid="in-10470" data-post-type="none"  >
						<a href="tel:+66658250131" class="wp_rp_thumbnail" >
						<img src="http://www.thai-kawaii-ageha.com/blog/wp-content/uploads/2018/03/hino_phone.jpg" w alt="↑「マリリン＆モンロー＠パンチラタイフーン」（事前Eメール予約はコチラ"  /></a>

					</li>




					<li data-position="0" data-poid="in-10470" data-post-type="none"  >
						<a href="tel:+66655902655" class="wp_rp_thumbnail" >
						<img src="http://www.thai-kawaii-ageha.com/blog/wp-content/uploads/2018/03/shun_phone.jpg" alt="↑「バンコク王子の成長物語」（事前Eメール予約はコチラ)"  /></a>

					</li>

					<li data-position="0" data-poid="in-10470" data-post-type="none" >
						<a href="tel:022337775" class="wp_rp_thumbnail" >
						<img src="http://www.thai-kawaii-ageha.com/blog/wp-content/uploads/2018/03/monroe_tel.jpg" alt="↑モンロー店舗 連絡先 電話番号 022-33-7775"  /></a>

					</li>

					</ul>
						</p>

					</div>


					<div style="text-align:center !important;">
						<a href="http://www.thai-kawaii-ageha.com/prince/contact/"><img src="http://www.thai-kawaii-ageha.com/prince/wp-content/uploads/2017/12/bangkok_prince.jpg"></a>
					</div>

<div id="menu_custom" class="p_t10">
	<div class="one">
		<a href="http://www.thai-kawaii-ageha.com/prince/category/bangkok/"><img src="http://thai-kawaii-ageha.com/prince/wp-content/uploads/2014/05/banner_side01.jpg"></a>
	</div>
	<div class="one">
		<a href="http://www.thai-kawaii-ageha.com/prince/category/tanya/"><img src="http://thai-kawaii-ageha.com/prince/wp-content/uploads/2014/05/banner_side02.jpg"></a>
	</div>
	<div class="one">
		<a href="http://www.thai-kawaii-ageha.com/prince/category/discovery/"><img src="http://thai-kawaii-ageha.com/prince/wp-content/uploads/2014/05/banner_side041.jpg"></a>
	</div>
	<div class="one">
		<a href="http://www.thai-kawaii-ageha.com/prince/category/girl/"><img src="http://thai-kawaii-ageha.com/prince/wp-content/uploads/2014/05/banner_side03.jpg"></a>
	</div>
	<div class="one">
		<a href="http://www.thai-kawaii-ageha.com/"><img src="http://www.thai-kawaii-ageha.com/prince/wp-content/uploads/2016/07/ageha_main.jpg"></a>
	</div>
	<div class="one">
		<a href="http://www.thai-kawaii-ageha.com/prince/contact/"><img src="http://www.thai-kawaii-ageha.com/prince/wp-content/uploads/2016/07/contact_banner.jpg"></a>
	</div>

</div>

<hr class="clear" /><div class="wp_rp_wrap  wp_rp_vertical_m" id="wp_rp_first"><div class="wp_rp_content"><h3 class="related_post_title">

					<?php if ( bauhaus_should_show_taxonomy() ) { ?>
						<?php if ( wptouch_has_categories() || wptouch_has_tags() ) { ?>
							<div class="cat-tags">
								<?php if ( wptouch_has_categories() ) { ?>
									<?php _e( 'Categories', 'wptouch-pro' ); ?>: <?php wptouch_the_categories(); ?><br />
								<?php } ?>
								<?php if ( wptouch_has_tags() ) { ?>
									<?php _e( 'Tags', 'wptouch-pro' ); ?>: <?php wptouch_the_tags(); ?>
								<?php } ?>
							</div>
						<?php } ?>

						<?php if ( wptouch_has_tags() ) { ?>
						<?php } ?>
					<?php } ?>
				</div>
			</div>

		<?php } ?>
	</div> <!-- content -->

	<?php do_action( 'wptouch_after_post_content' ); ?>

	<?php get_template_part( 'nav-bar' ); ?>
	<?php if ( comments_open() || wptouch_have_comments() ) { ?>
		<div id="comments">
			<?php comments_template(); ?>
		</div>
	<?php } ?>

<?php get_footer(); ?>
