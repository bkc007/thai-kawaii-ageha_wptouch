<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<title><?php wp_title( ' | ', true, 'right' ); ?></title>
		<?php wptouch_head(); ?>
		<?php
			if ( !is_single() && !is_archive() && !is_page() && !is_search() ) {
				wptouch_canonical_link();
			}

			if ( isset( $_REQUEST[ 'wptouch_preview_theme' ] ) || isset( $_REQUEST[ 'wptouch_switch' ] ) )  {
				echo '<meta name="robots" content="noindex" />';
			}
		?>



<style type='text/css'>

.relatedthumb {
    width: 46% !important;
    height: 110px !important;
}


.yuzo_wraps {
    margin-left: 5px !important;
}



.wp_rp_thumbnail {
	width: 145px !important;
}



ul.related_post {
	display: inline-block !important;
	position: relative !important;
	margin: 0 !important;
	padding: 0 !important;
}
ul.related_post li {
	position: relative !important;
	display: inline-block !important;
	vertical-align: top !important;
	zoom: 1 !important;
	*display: inline !important;
	width: 32% !important;
	margin: 0px 0px 0px 0px !important;
	padding: 0 !important;
	background: none !important;
}
ul.related_post li a {
	position: relative !important;
	display: block !important;
	font-size: 13px !important;
	line-height: 1.6em !important;
	text-decoration: none !important;
	margin-bottom: 5px !important;
	text-indent: 0 !important;
}
ul.related_post li img {
	display: block !important;
	width: 32% !important;
	max-width: 100% !important;
	margin: 0 !important;
	padding: 0 !important;
	background: none !important;
	border: none !important;

	border-radius: 3px !important;
	box-shadow: 0 1px 4px rgba(0, 0, 0, 0.2) !important;
}

ul.related_post li a img.wp_rp_santa_hat {
	display: none !important;
}

ul.related_post li small {
	font-size: 80%;
}

@media screen and (max-width: 480px) {
	ul.related_post li {
		display: inline-block !important;
		width: 32% !important;
		clear: both !important;
	}
	ul.related_post li a:nth-child(1) {
		float: left !important;
	}
	ul.related_post li a:nth-child(2) {
		font-size: 14px !important;
	}
}

ul.related_post li .wp_rp_category a {
	display: inline !important;
}



ul.related_post li a {
		line-height: 1.3em !important;
		width: 100% !important;
}



ul.related_post li img {
		width: 100% !important;
}





ul.related_post li img {
	display: block !important;
	width: 100% !important;
	height: auto !important;
	max-width: 100% !important;
	margin: 0 !important;
	padding: 0 !important;
	background: none !important;
	border: none !important;

	border-radius: 3px !important;
	box-shadow: 0 1px 4px rgba(0, 0, 0, 0.2) !important;
}




body [class^="wptouch-icon-"]::before, body [class*=" wptouch-icon-"]::before {
    margin-top: 7px !important;
}


</style>




	</head>

	<body <?php body_class( wptouch_get_body_classes() ); ?>>

		<?php do_action( 'wptouch_preview' ); ?>

		<?php do_action( 'wptouch_body_top' ); ?>

		<?php get_template_part( 'header-bottom' ); ?>

		<?php do_action( 'wptouch_body_top_second' ); ?>
