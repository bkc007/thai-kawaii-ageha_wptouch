<?php
register_sidebar(array(
'before_widget' => '<div id="%1$s" class="widget %2$s">',
'after_widget' => '</div>',
'before_title' => '<h3>',
'after_title' => '</h3>',
));


register_sidebar( array(
'name' => 'フッター１',
'id' => 'footer_1',
'description' => '新規ウィジェットエリア',
'before_widget' => '',
'after_widget' => '',
) );

register_nav_menus (

	array(
		"footernav" => 'フッターナビ'

));


remove_action('wp_head','rest_output_link_wp_head');
remove_action('wp_head','wp_oembed_add_discovery_links');
remove_action('wp_head','wp_oembed_add_host_js');



//管理画面での記事一覧カラムカスタマイズ
//WordpressPoplarPostのビューカウントを表示する
function admin_posts_columns($columns) {
    $columns['subtitle'] = "ビュー";
    return $columns;
}
function add_admincolumn($column_name, $post_id) {
    if( $column_name == 'subtitle' ) {
        echo wpp_get_views($post_id, 'all', true);
    }
}
if ( function_exists('wpp_get_views') ) {
    add_filter( 'manage_posts_columns', 'admin_posts_columns' );
    add_action( 'manage_posts_custom_column', 'add_admincolumn', 10, 2 );
}



function admin_pages_columns($columns) {
    $columns['subtitle'] = "ビュー";
    return $columns;
}



?>
