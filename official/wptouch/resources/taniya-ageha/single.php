<?php get_header(); ?>

 <style>
 
 .wpcf7-form-control.wpcf7-textarea.wpcf7-validates-as-required {
    width: 400px !important;
    height: 300px !important;
    font-size: 18px !important;
}

.wpcf7-form-control.wpcf7-text {
    height: 25px !important;
    font-size: 16px !important;
}
 
 </style>
 

<div id="main" class="p_t20">



<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<h2 class="entry-title"><?php the_title();?></h2>

<div id="date" class="m_l10 p_t10"><span class="date update"><?php the_date('Y-m-d'); ?></span></div> 

<?php the_content(); ?>

<!--<div id="date">カテゴリー: <?php the_category(', '); ?>　<?php the_tags('タグ: ', ', '); ?></div>-->

<div id="next">
<?php previous_post_link('←「%link」前の記事へ　'); ?>
<?php next_post_link('　次の記事へ「%link」→'); ?>

<span class="vcard author"><span class="fn">Ageha</span></span>

</div>


<?php endwhile; else: ?>

<p><?php echo "お探しの記事、ページは見つかりませんでした。"; ?></p>

<?php endif; ?>

</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
