<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


<div class="breadcrumbs">
    <?php if(function_exists('bcn_display'))
    {
        bcn_display();
    }?>
</div>


<?php the_content(); ?>

<?php endwhile; else: ?>

<?php endif; ?>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
