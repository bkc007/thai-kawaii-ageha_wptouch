<?php
require_once('DB.php');

require_once('MDB2.php');

//main select(テーブルカラム全部呼び出し)

function main_select($db_access,$set_column,$table,$column,$where,$sort){

	$data = array();
	//$dsn ="pgsql://postgres:lunablesssql@localhost/rent_DB";
	$dsn = "mysqli://$db_access[0]:$db_access[1]@$db_access[2]/$db_access[3]";
	$db = MDB2::connect($dsn);

	//echo "SELECT $set_column from $table $where order by $column $sort <br />";

	$db->query('SET NAMES utf8');
	
	//echo "SELECT $set_column from $table $where order by $column $sort<br />";
	
	//$sql = $db->prepare("SELECT $set_column from $table $where order by $column $sort");
	//$res = $db->execute($sql);

	$res = $db->query("SELECT $set_column from $table $where order by $column $sort");

		$i = 0;		
		
		//echo "SELECT $set_column from $table $where order by $column $sort";

		while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
				
				foreach( $row as $key => $value ){	
				
				$data[$i][$key] = $value;

				}
				$i++;
		}

	$db->disconnect();

	return $data;
	
}


function dairy_select($db_access,$set_column,$table,$column,$where,$sort){

	$data = array();
	//$dsn ="pgsql://postgres:lunablesssql@localhost/rent_DB";
	$dsn = "mysqli://$db_access[0]:$db_access[1]@$db_access[2]/$db_access[3]";
	$db = MDB2::connect($dsn);

	//echo "SELECT $set_column from $table $where order by $column $sort <br />";

	$db->query('SET NAMES utf8');
	
	//echo "SELECT $set_column from $table $where order by $column $sort<br />";
	
	//$sql = $db->prepare("SELECT $set_column from $table $where order by $column $sort");
	//$res = $db->execute($sql);

	$res = $db->query("SELECT $set_column from $table $where");

		$i = 0;		
		
		//echo "SELECT $set_column from $table $where order by $column $sort";

		while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
				
				foreach( $row as $key => $value ){	
				
				$data[$i][$key] = $value;

				}
				$i++;
		}

	$db->disconnect();

	return $data;
	
}





##製品件数カウント
function client_count_select($db_access,$where,$search){

	$data = array();

	$dsn = "mysqli://$db_access[0]:$db_access[1]@$db_access[2]/$db_access[3]";
	$db = MDB2::connect($dsn);

	//echo "SELECT count(*) as count from admin_tb $where";

	$db->query('SET NAMES utf8');

	
	//期間の絞り込み
	if($search['admin_info_regist_min'] != NULL){


		if($where != NULL){
			}else{
			$where .= " WHERE";
		}



		$where .= " (admin_info_regist between 
		".$db->Quote($db->escapePattern($search['admin_info_regist_min']))." AND ".$db->Quote($db->escapePattern($search['admin_info_regist_max'])).") ";	

	}


	//ユーザー登録メールの検索
	if($search['admin_info_mail'] != NULL){


		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}



		$where .= " admin_info_mail like ".$db->Quote("%".$db->escapePattern($search['admin_info_mail'])."%");	
					
	}

	//ユーザーの名前検索
	if($search['admin_info_company_name'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}


		$where .= " admin_info_company_name like ".$db->Quote("%".$db->escapePattern($search['admin_info_company_name'])."%");	
					
	}

	//クライアントの権限
	if($search['admin_limit'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}

	$where .= " admin_limit = ".$db->Quote($db->escapePattern($search['admin_limit']))." ";	


	}


	//クライアントステータス
	if($search['admin_del'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}

	$where .= " admin_del = ".$db->Quote($db->escapePattern($search['admin_del']))." ";	


	}




	/*echo "SELECT admin_tb.*,admin_info_tb.*,count(*) as count from admin_tb 
	LEFT JOIN admin_info_tb ON
	admin_tb.admin_id = admin_info_tb.admin_attach_id
	".$where;*/

	
	$res = $db->query("SELECT admin_tb.*,admin_info_tb.*,count(*) as count from admin_tb 
	LEFT JOIN admin_info_tb ON
	admin_tb.admin_id = admin_info_tb.admin_attach_id
	$where");
	

		$i = 0;		
		
	//var_dump($res);

		while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
				
				foreach( $row as $key => $value ){	
				
				$data[$i][$key] = $value;
				
				}

				$i++;
				
		
		}
	


	return $data[0]['count'];
	
}


##お問い合わせのカウント
function contact_count_select($db_access,$where,$search){
	
	
	 //var_dump($search);

	$data = array();

	$dsn = "mysqli://$db_access[0]:$db_access[1]@$db_access[2]/$db_access[3]";
	$db = MDB2::connect($dsn);

	//echo "SELECT count(*) as count from admin_tb $where";

	$db->query('SET NAMES utf8');

	
	//期間の絞り込み
	if($search['contact_date_min'] != NULL){


		if($where != NULL){

			$where .= " AND ";
			
			}else{
			$where .= " WHERE ";
		}



		$where .= "  ( contact_date between 
		".$db->Quote($db->escapePattern($search['contact_date_min']))." AND ".$db->Quote($db->escapePattern($search['contact_date_max'])).") ";	

	}
	
	//期間の絞り込み
	if($search['charge_date_min'] != NULL){


		if($where != NULL){

			$where .= " AND ";
			
			}else{
			$where .= " WHERE ";
		}



		$where .= "  ( auth_date between 
		".$db->Quote($db->escapePattern($search['charge_date_min']))." AND ".$db->Quote($db->escapePattern($search['charge_date_max'])).") ";	

	}
		
	
	


	//ユーザーの名前検索
	if($search['admin_info_company_name'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE ";
		}


		$where .= " admin_info_company_name like ".$db->Quote("%".$db->escapePattern($search['admin_info_company_name'])."%");	
					
	}

	//認証の検索
	if($search['auth_check'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE ";
		}

	$where .= " auth_check = ".$db->Quote($db->escapePattern($search['auth_check']))." ";	


	}
	
	//認証の検索
	if($search['category_id'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE ";
		}

	$where .= " category_id = ".$db->Quote($db->escapePattern($search['category_id']))." ";	


	}	

	//会社IDのアタッチ	
	if($search['company_id'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}

	$where .= " admin_attach_id = ".$db->Quote($db->escapePattern($search['company_id']))." ";	


	}		



	$res = $db->query("                           
									SELECT count(*) as count, contact_tb.*,user_info_tb.*,admin_info_tb.*,contact_auth_tb.* ,category_tb.* from
									contact_tb 
									LEFT JOIN user_info_tb ON user_info_tb.user_attach_id = contact_tb.contact_user_attach
									LEFT JOIN admin_info_tb ON admin_info_tb.admin_attach_id = contact_tb.contact_data_attach		
		  							LEFT JOIN contact_auth_tb ON contact_auth_tb.auth_contact_attach = contact_tb.contact_id
			  						LEFT JOIN category_tb ON category_tb.category_id = contact_tb.contact_category
										
									$where $sort_set $sort");
	
	/*echo "SELECT contact_tb.*,user_info_tb.*,admin_info_tb.*,contact_auth_tb.* ,data_tb.* ,category_tb.* from
									contact_tb 
									LEFT JOIN user_info_tb ON user_info_tb.user_attach_id = contact_tb.contact_user_attach
									LEFT JOIN admin_info_tb ON admin_info_tb.admin_attach_id = contact_tb.contact_data_attach		
		  							LEFT JOIN contact_auth_tb ON contact_auth_tb.auth_contact_attach = contact_tb.contact_id
		  							LEFT JOIN data_tb ON data_tb.data_attach_admin_id = contact_tb.contact_data_attach
		  							LEFT JOIN category_tb ON category_tb.category_id = data_tb.data_category_id".
										
									$where.' '.$sort_set.' '.$sort;*/
	

		$i = 0;		
		
	//var_dump($res);

		while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
				
				foreach( $row as $key => $value ){	
				
				$data[$i][$key] = $value;
				
				}

				$i++;
				
		
		}
	


	return $data[0]['count'];
	
}



##カテゴリ件数
function category_count_select($db_access,$where,$search){

	$data = array();

	$dsn = "mysqli://$db_access[0]:$db_access[1]@$db_access[2]/$db_access[3]";
	$db = MDB2::connect($dsn);

	//echo "SELECT $set_column from $table $where order by $column $sort";

	$db->query('SET NAMES utf8');

	
	//業種による絞りこみ
	
			//////var_dump($search['kind_type']);


	$res = $db->query("SELECT count(*) as count from category_tb $where");
	

		$i = 0;		
		
	//var_dump($res);

		while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
				
				foreach( $row as $key => $value ){	
				
				$data[$i][$key] = $value;
				
				}

				$i++;
				
		
		}
	


	return $data[0]['count'];
	
}



##登録データかうんと
function data_count_select($db_access,$where,$search){

	$data = array();

	$dsn = "mysqli://$db_access[0]:$db_access[1]@$db_access[2]/$db_access[3]";
	$db = MDB2::connect($dsn);

	//echo "SELECT $set_column from $table $where order by $column $sort";

	$db->query('SET NAMES utf8');

	
	//業種による絞りこみ
	
			//////var_dump($search['kind_type']);


	$res = $db->query("SELECT count(*) as count from data_tb $where");
	


	//echo "SELECT count(*) as count from data_tb $where";


		$i = 0;		
		
	//var_dump($res);

		while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
				
				foreach( $row as $key => $value ){	
				
				$data[$i][$key] = $value;
				
				}

				$i++;
				
		
		}
	


	return $data[0]['count'];
	
}


##請求データ絞り込み
function contact_select($db_access,$where,$sort_set,$sort,$search){




	$data = array();

	$dsn = "mysqli://$db_access[0]:$db_access[1]@$db_access[2]/$db_access[3]";
	$db = MDB2::connect($dsn);

	//echo "SELECT $set_column from $table $where order by $column $sort";

	$db->query('SET NAMES utf8');

	

	//期間の絞り込み
	if($search['contact_date_min'] != NULL){


		if($where != NULL){
			$where .= " AND ";		
			}else{
			$where .= " WHERE";
		}



		$where .= " (contact_date between 
		".$db->Quote($db->escapePattern($search['contact_date_min']))." AND ".$db->Quote($db->escapePattern($search['contact_date_max'])).") ";	

	}


	//ユーザーの名前検索
	if($search['admin_info_company_name'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}


		$where .= " admin_info_company_name like ".$db->Quote("%".$db->escapePattern($search['admin_info_company_name'])."%");	
					
	}

	//認証の検索
	if($search['auth_check'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}

	$where .= " auth_check = ".$db->Quote($db->escapePattern($search['auth_check']))." ";	


	}
	
	//認証の検索
	if($search['category_id'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}

	$where .= " category_id = ".$db->Quote($db->escapePattern($search['category_id']))." ";	


	}	
	
	//会社IDのアタッチ	
	if($search['company_id'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}

	$where .= " admin_attach_id = ".$db->Quote($db->escapePattern($search['company_id']))." ";	


	}		


	
	//期間の絞り込み
	if($search['charge_date_min'] != NULL){


		if($where != NULL){

			$where .= " AND ";
			
			}else{
			$where .= " WHERE ";
		}



		$where .= "  ( auth_date between 
		".$db->Quote($db->escapePattern($search['charge_date_min']))." AND ".$db->Quote($db->escapePattern($search['charge_date_max'])).") ";	

	}
		




	$res = $db->query("                           
									SELECT contact_tb.*,user_info_tb.*,admin_info_tb.*,contact_auth_tb.* ,category_tb.* from
									contact_tb 
									LEFT JOIN user_info_tb ON user_info_tb.user_attach_id = contact_tb.contact_user_attach
									LEFT JOIN admin_info_tb ON admin_info_tb.admin_attach_id = contact_tb.contact_data_attach		
		  							LEFT JOIN contact_auth_tb ON contact_auth_tb.auth_contact_attach = contact_tb.contact_id
			  						LEFT JOIN category_tb ON category_tb.category_id = contact_tb.contact_category
										
									$where $sort_set $sort");
	
	/*echo "SELECT contact_tb.*,user_info_tb.*,admin_info_tb.*,contact_auth_tb.* ,data_tb.* ,category_tb.* from
									contact_tb 
									LEFT JOIN user_info_tb ON user_info_tb.user_attach_id = contact_tb.contact_user_attach
									LEFT JOIN admin_info_tb ON admin_info_tb.admin_attach_id = contact_tb.contact_data_attach		
		  							LEFT JOIN contact_auth_tb ON contact_auth_tb.auth_contact_attach = contact_tb.contact_id
		  							LEFT JOIN data_tb ON data_tb.data_attach_admin_id = contact_tb.contact_data_attach
		  							LEFT JOIN category_tb ON category_tb.category_id = data_tb.data_category_id".
										
									$where.' '.$sort_set.' '.$sort;*/


		$i = 0;		
		
	//var_dump($res);

		while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
				
				foreach( $row as $key => $value ){	
				
				$data[$i][$key] = $value;
				
				}

				$i++;
				
		
		}
	


	return $data;
	
}




##メンバー数カウント
function user_count_select($db_access,$where,$search){

	$data = array();

	$dsn = "mysqli://$db_access[0]:$db_access[1]@$db_access[2]/$db_access[3]";
	$db = MDB2::connect($dsn);

	//echo "SELECT $set_column from $table $where order by $column $sort";

	$db->query('SET NAMES utf8');


	//期間の絞り込み
	if($search['user_info_regist_min'] != NULL){


		if($where != NULL){
			}else{
			$where .= " WHERE";
		}



		$where .= " (user_info_regist between 
		".$db->Quote($db->escapePattern($search['user_info_regist_min']))." AND ".$db->Quote($db->escapePattern($search['user_info_regist_max'])).") ";	

	}



	//ユーザーの名前検索
	if($search['user_info_name'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}


		$where .= " user_info_name like ".$db->Quote("%".$db->escapePattern($search['user_info_name'])."%");	
					
	}
	

	
	//ユーザーの名前検索
	if($search['user_info_name_kana'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}


		$where .= " user_info_name_kana like ".$db->Quote("%".$db->escapePattern($search['user_info_name_kana'])."%");	
					
	}	

	//ユーザーの性別
	if($search['user_info_gender'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}


		$where .= " user_info_gender = '".$search['user_info_gender']."' ";	
					
	}		
	
	
	//ユーザーの会員ナンバー
	if($search['user_info_member_number'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}


		$where .= " user_info_member_number like ".$db->Quote("%".$db->escapePattern($search['user_info_member_number'])."%");	
					
	}	

	//ユーザーの電話番号
	if($search['user_info_tel'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}


		$where .= " user_info_tel like ".$db->Quote("%".$db->escapePattern($search['user_info_tel'])."%");	
					
	}
	


	//ユーザーの誕生日
	
	$date_birth = NULL;
	$set_format = NULL;
	
	if($search['birth_dayYear'] != NULL){

		$year = 1;
		$date_birth .= $search['birth_dayYear']; 
		$set_format .="%Y";
		
	}
	if($search['birth_dayMonth'] != NULL){
		$month = 1;
		$date_birth .= $search['birth_dayMonth']; 
		$set_format .="%m";
		
	}
	if($search['birth_dayDay'] != NULL){
		$day = 1;
		$date_birth .= $search['birth_dayDay']; 
		$set_format .="%d";
		
	}	
		if($date_birth != NULL){

			if($where != NULL){
				$where .= " AND ";
			}else{
				$where .= " WHERE";
			}
			
			$where .= " (DATE_FORMAT(user_info_birthday, '".$set_format."') = '".$date_birth."' ) ";
		
		}

	
		//ユーザーの来店日
	$date_come = NULL;
	$set_format = NULL;
	
	if($search['come_Year'] != NULL){

		$year = 1;
		$date_come .= $search['come_Year']; 
		$set_format .="%Y";
		
	}
	if($search['come_Month'] != NULL){
		$month = 1;
		$date_come .= $search['come_Month']; 
		$set_format .="%m";
		
	}
	if($search['come_Day'] != NULL){
		$day = 1;
		$date_come .= $search['come_Day']; 
		$set_format .="%d";
		
	}	
		if($date_come != NULL){

			if($where != NULL){
				$where .= " AND ";
			}else{
				$where .= " WHERE";
			}
			
			$where .= " (DATE_FORMAT(user_info_regist, '".$set_format."') = '".$date_come."' ) ";
		
		}

	
	 //var_dump($where);

	//echo 'SELECT  count(*) as count from user_info_tb'.$where;


	
		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}

	$where .= " user_info_del =  0 ";	



	$res = $db->query("SELECT  count(*) as count from user_info_tb 
	 $where");
	

		$i = 0;		
		
	//var_dump($res);

		while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
				
				foreach( $row as $key => $value ){	
				
				$data[$i][$key] = $value;
				
				}

				$i++;
				
		
		}
	


	return $data[0]['count'];
	
}





##業者

function client_select($db_access,$where,$sort_set,$sort,$search){

	$data = array();

	$dsn = "mysqli://$db_access[0]:$db_access[1]@$db_access[2]/$db_access[3]";
	$db = MDB2::connect($dsn);

	/*echo "SELECT admin_tb.*,admin_info_tb.* from admin_tb 
	LEFT JOIN admin_info_tb ON
	admin_tb.admin_id = admin_info_tb.admin_attach_id
	$where";*/

	$db->query('SET NAMES utf8');



	//期間の絞り込み
	if($search['admin_info_regist_min'] != NULL){


		if($where != NULL){
			}else{
			$where .= " WHERE";
		}



		$where .= " (admin_info_regist between 
		".$db->Quote($db->escapePattern($search['admin_info_regist_min']))." AND ".$db->Quote($db->escapePattern($search['admin_info_regist_max'])).") ";	

	}


	//ユーザー登録メールの検索
	if($search['admin_info_mail'] != NULL){


		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}



		$where .= " admin_info_mail like ".$db->Quote("%".$db->escapePattern($search['admin_info_mail'])."%");	
					
	}

	//ユーザーの名前検索
	if($search['admin_info_company_name'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}


		$where .= " admin_info_company_name like ".$db->Quote("%".$db->escapePattern($search['admin_info_company_name'])."%");	
					
	}

	//クライアントの権限
	if($search['admin_limit'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}

	$where .= " admin_limit = ".$db->Quote($db->escapePattern($search['admin_limit']))." ";	


	}


	//クライアントステータス
	if($search['admin_del'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}

	$where .= " admin_del = ".$db->Quote($db->escapePattern($search['admin_del']))." ";	


	}


	//var_dump($where);

	/*echo  "SELECT admin_tb.*,admin_info_tb.* from admin_tb 
	LEFT JOIN admin_info_tb ON
	admin_tb.admin_id = admin_info_tb.admin_attach_id
	" .$where." ".$sort;*/
	
	
	$res = $db->query("SELECT admin_tb.*,admin_info_tb.* from admin_tb 
	LEFT JOIN admin_info_tb ON
	admin_tb.admin_id = admin_info_tb.admin_attach_id
	$where $sort");
	

		$i = 0;		
		

		while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
				
				foreach( $row as $key => $value ){	
				
				$data[$i][$key] = $value;				
				
				}
				
				$data_set = main_select($db_access,"count(*) as count ","data_tb","data_id",
				" WHERE data_attach_admin_id = ".$data[$i]['admin_attach_id']."","desc");
				
				
				$data[$i]['data_set'] = $data_set[0]['count'];


		$i++;
		
		
		}
	


	return $data;
	
}




##カテゴリ
function category_select($db_access,$where,$sort_set,$sort,$search){

	$data = array();

	$dsn = "mysqli://$db_access[0]:$db_access[1]@$db_access[2]/$db_access[3]";
	$db = MDB2::connect($dsn);

	//echo "SELECT * from category_tb $where";

	$db->query('SET NAMES utf8');

	
	$res = $db->query("SELECT * from category_tb $where");
	

		$i = 0;		
		

		while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
				
				foreach( $row as $key => $value ){	
				
				$data[$i][$key] = $value;
				
				}

		$i++;
		
		
		}
	

	return $data;
	
}


##クライアント登録データ呼び出し
function data_select($db_access,$where,$sort_set,$sort,$search){

	$data = array();

	$dsn = "mysqli://$db_access[0]:$db_access[1]@$db_access[2]/$db_access[3]";
	$db = MDB2::connect($dsn);

	//echo "SELECT * from category_tb $where";

	$db->query('SET NAMES utf8');


	/*echo 'SELECT data_tb.*,category_tb.*,admin_info_tb . * from data_tb 
									LEFT JOIN category_tb ON category_id = data_category_id 
									LEFT JOIN admin_info_tb ON data_attach_admin_id = admin_attach_id	'.								
									$where;*/

	
	$res = $db->query("SELECT data_tb.*,category_tb.*,admin_info_tb . * from data_tb 
									LEFT JOIN category_tb ON category_id = data_category_id 
									LEFT JOIN admin_info_tb ON data_attach_admin_id = admin_attach_id									
									$where");
	

		$i = 0;		
		

		while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
				
				foreach( $row as $key => $value ){	
				
				$data[$i][$key] = $value;
				
				}

		$i++;
		
		
		}
	

	return $data;
	
}

##クライアント登録データ呼び出し
function data_select_web($db_access,$where,$sort_set,$sort,$search){

	$data = array();

	$dsn = "mysqli://$db_access[0]:$db_access[1]@$db_access[2]/$db_access[3]";
	$db = MDB2::connect($dsn);

	//echo "SELECT * from category_tb $where";

	$db->query('SET NAMES utf8');


	/*echo 'SELECT data_tb.*,region_tb.*,category_tb.*,admin_info_tb . *  from region_tb 
									LEFT JOIN data_tb ON data_id = region_data_attacch
									LEFT JOIN category_tb ON data_category_id = category_id
									LEFT JOIN admin_info_tb ON data_attach_admin_id = admin_attach_id									
									'.$where;
	*/
	
	$res = $db->query("SELECT data_tb.*,region_tb.*,category_tb.*,admin_info_tb . *  from region_tb 
									LEFT JOIN data_tb ON data_id = region_data_attacch
									LEFT JOIN category_tb ON data_category_id = category_id
									LEFT JOIN admin_info_tb ON data_attach_admin_id = admin_attach_id									
									$where");
	

		$i = 0;		
		

		while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
				
				foreach( $row as $key => $value ){	
				
				$data[$i][$key] = $value;
				
				}

		$i++;
		
		
		}
	

	return $data;
	
}


##掲載業者登録データ呼び出し
function client_select_web($db_access,$where,$sort_set,$sort,$search){

	$data = array();

	$dsn = "mysqli://$db_access[0]:$db_access[1]@$db_access[2]/$db_access[3]";
	$db = MDB2::connect($dsn);

	//echo "SELECT * from category_tb $where";

	$db->query('SET NAMES utf8');


	/*echo 'SELECT data_tb.*,category_tb.*,admin_info_tb . *  from data_tb 
									LEFT JOIN category_tb ON data_category_id = category_id
									LEFT JOIN admin_info_tb ON data_attach_admin_id = admin_attach_id			
									'.$where;
	*/
	
	$res = $db->query("SELECT data_tb.*,category_tb.*,admin_info_tb . *  from data_tb 
									LEFT JOIN category_tb ON data_category_id = category_id
									LEFT JOIN admin_info_tb ON data_attach_admin_id = admin_attach_id		
									$where");
	

		$i = 0;		
		

		while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
				
				foreach( $row as $key => $value ){	
				
				$data[$i][$key] = $value;
				
				}

		$i++;
		
		
		}
	

	return $data;
	
}




##会員情報選択
function user_select($db_access,$where,$sort_set,$sort,$search){

	$data = array();

	$dsn = "mysqli://$db_access[0]:$db_access[1]@$db_access[2]/$db_access[3]";
	$db = MDB2::connect($dsn);

	/*echo "SELECT user_tb.*,user_info_tb.* from user_tb 
	INNER JOIN user_info_tb ON user_info_tb.user_attach_id = user_tb.user_id 
	$where";
	*/
	$db->query('SET NAMES utf8');



	//ユーザーの名前検索
	if($search['user_info_name'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}


		$where .= " user_info_name like ".$db->Quote("%".$db->escapePattern($search['user_info_name'])."%");	
					
	}
	
	//ユーザーの名前検索
	if($search['user_info_name_kana'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}


		$where .= " user_info_name_kana like ".$db->Quote("%".$db->escapePattern($search['user_info_name_kana'])."%");	
					
	}	

	//ユーザーの性別
	if($search['user_info_gender'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}


		$where .= " user_info_gender = '".$search['user_info_gender']."' ";	
					
	}		
	

	//ユーザーの電話番号
	if($search['user_info_tel'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}


		$where .= " user_info_tel like ".$db->Quote("%".$db->escapePattern($search['user_info_tel'])."%");	
					
	}
	
	
	//ユーザーの会員ナンバー
	if($search['user_info_member_number'] != NULL){

		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}


		$where .= " user_info_member_number like ".$db->Quote("%".$db->escapePattern($search['user_info_member_number'])."%");	
					
	}	




	//ユーザーの誕生日
	
	$date_birth = NULL;
	$set_format = NULL;
	
	if($search['birth_dayYear'] != NULL){

		$year = 1;
		$date_birth .= $search['birth_dayYear']; 
		$set_format .="%Y";
		
	}
	if($search['birth_dayMonth'] != NULL){
		$month = 1;
		$date_birth .= $search['birth_dayMonth']; 
		$set_format .="%m";
		
	}
	if($search['birth_dayDay'] != NULL){
		$day = 1;
		$date_birth .= $search['birth_dayDay']; 
		$set_format .="%d";
		
	}	
		if($date_birth != NULL){

			if($where != NULL){
				$where .= " AND ";
			}else{
				$where .= " WHERE";
			}
			
			$where .= " (DATE_FORMAT(user_info_birthday, '".$set_format."') = '".$date_birth."' ) ";
		
		}

	
		//ユーザーの来店日
	$date_come = NULL;
	$set_format = NULL;
	
	if($search['come_Year'] != NULL){

		$year = 1;
		$date_come .= $search['come_Year']; 
		$set_format .="%Y";
		
	}
	if($search['come_Month'] != NULL){
		$month = 1;
		$date_come .= $search['come_Month']; 
		$set_format .="%m";
		
	}
	if($search['come_Day'] != NULL){
		$day = 1;
		$date_come .= $search['come_Day']; 
		$set_format .="%d";
		
	}	
		if($date_come != NULL){

			if($where != NULL){
				$where .= " AND ";
			}else{
				$where .= " WHERE";
			}
			
			$where .= " (DATE_FORMAT(user_info_regist, '".$set_format."') = '".$date_come."' ) ";
		
		}

	
	
		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}

	$where .= " user_info_del =  0 ";	
	
	

	
	//echo 'SELECT * from user_info_tb'.$where.$sort.$sort_set;

	
	$res = $db->query("SELECT * from user_info_tb 
	$where $sort_set $sort");
	

		$i = 0;		
		$z = 0;		

		while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
				
				foreach( $row as $key => $value ){	
				
				$data[$i][$key] = $value;
				
				}

			//カテゴリをアタッチする
			$z = 0;
			$category_data = main_select($db_access,'*','category_code_tb','category_code_id',' where category_code_attach_id = '.$data[$i]['user_info_id'],$sort);

			
			foreach ($category_data as $category){
			
			   $data[$i]['category_set'][$z] =	 main_select($db_access,'*','category_tb','category_id',' where category_id = '.$category['category_code_name'],$sort);

				$z++;
			}

			$i++;
		
		
		}
	
	

	

	return $data;
	
}



//ｱﾄﾞﾐﾝデータ呼び出し

function user_info_select($db_access,$user_id){

	
						$where =" where user_info_attach_id ='".$user_id."'";
						//表示セット
						$set_column = "user_info_tb.*,user_tb.* ";
						$sort = 'asc';
					
						//end---------------------
							
						$user_info_data =  main_select($db_access,$set_column,
						'user_info_tb LEFT JOIN user_tb ON user_info_tb.user_info_attach_id = user_tb.user_id ',
						'user_info_id',$where,$sort);


	return $user_info_data;
	
}


function login_user_select($db_access,$user_id){

	
						$where ="where user_attach_id ='".$user_id."'";
						$user_data =  main_select($db_access,"*",'user_info_tb','user_info_id',$where,"desc");

	return $user_data;
	
}






##一括請求の呼び出し
function charge_select($db_access,$where,$sort_set,$sort,$search){

	$data = array();

	$dsn = "mysqli://$db_access[0]:$db_access[1]@$db_access[2]/$db_access[3]";
	$db = MDB2::connect($dsn);


	$db->query('SET NAMES utf8');



	//ユーザー登録メールの検索
	/*if($search['admin_info_mail'] != NULL){


		if($where != NULL){
			$where .= " AND ";
		}else{
			$where .= " WHERE";
		}



		$where .= " admin_info_mail like ".$db->Quote("%".$db->escapePattern($search['admin_info_mail'])."%");	
					
	}*/
	
	
	//echo "SELECT * from charge_tb ".$where;
	
	
	$res = $db->query("SELECT * from charge_tb ".$where);
	

		$i = 0;		
		

		while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
				
				foreach( $row as $key => $value ){	
				
				$data[$i][$key] = $value;				
				
				}

		//日付の月末取得方法
		$date = explode("-", $data[$i]['charge_contact_month']);
		
		$data[$i]['charge_contact_month_end'] = date('Y-m-d', mktime(0, 0, 0, date($date[1]) + 1, 0, date($date[0])));		

		$i++;
		
		
		}
	


	return $data;
	
}









?>