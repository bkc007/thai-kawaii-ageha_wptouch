<?php

add_action( 'foundation_enqueue_scripts', 'bauhaus_enqueue_scripts' );
add_filter( 'amp_should_show_featured_image_in_header', 'bauhaus_should_show_thumbnail' );
add_filter( 'foundation_featured_show', 'bauhaus_show_featured_slider', 10, 2 );

function bauhaus_enqueue_scripts() {
	wp_enqueue_script(
		'bauhaus-js',
		BAUHAUS_URL . '/default/bauhaus.js',
		array( 'jquery' ),
		BAUHAUS_THEME_VERSION,
		true
	);
}

function bauhaus_should_show_thumbnail() {
	$settings = bauhaus_get_settings();

	switch( $settings->bauhaus_use_thumbnails ) {
		case 'none':
			return false;
		case 'index':
			return is_home();
		case 'index_single':
			return is_home() || is_single();
		case 'index_single_page':
			return is_home() || is_single() || is_page();
		case 'all':
			return is_home() || is_single() || is_page() || is_archive() || is_search();
		default:
			// in case we add one at some point
			return false;
	}
}

function bauhaus_should_show_taxonomy() {
	$settings = bauhaus_get_settings();

	if ( $settings->bauhaus_show_taxonomy ) {
		return true;
	} else {
		return false;
	}
}

function bauhaus_should_show_date(){
	$settings = bauhaus_get_settings();

	if ( $settings->bauhaus_show_date ) {
		return true;
	} else {
		return false;
	}
}

function bauhaus_should_show_author(){
	$settings = bauhaus_get_settings();

	if ( $settings->bauhaus_show_author ) {
		return true;
	} else {
		return false;
	}
}

function bauhaus_should_show_search(){
	$settings = bauhaus_get_settings();

	if ( $settings->bauhaus_show_search ) {
		return true;
	} else {
		return false;
	}
}

function bauhaus_should_show_comment_bubbles(){
	$settings = bauhaus_get_settings();

	if ( $settings->bauhaus_show_comment_bubbles ) {
		return true;
	} else {
		return false;
	}
}

function bauhaus_is_menu_position_default(){
	$settings = bauhaus_get_settings();

	if ( $settings->bauhaus_menu_position == 'left-side' ) {
		return true;
	} else {
		return false;
	}
}

function bauhaus_show_featured_slider( $show_featured_slider, $featured_slider_enabled ) {
	if ( bauhaus_allow_featured_slider_override() ) {
		$settings = bauhaus_get_settings();

		global $post;
		if ( $settings->featured_slider_page !== false && $post->ID == $settings->featured_slider_page ) {
			$show_featured_slider = true;
		} elseif ( $settings->featured_slider_page == true ) {
			$show_featured_slider = false;
		}
	}

	return $show_featured_slider;
}


function is_mobile () {
$useragents = array(
'iPhone', // Apple iPhone
'iPod', // Apple iPod touch
'iPad', // Apple iPad
'Android', // 1.5+ Android
'dream', // Pre 1.5 Android
'CUPCAKE', // 1.5+ Android
'blackberry9500', // Storm
'blackberry9530', // Storm
'blackberry9520', // Storm v2
'blackberry9550', // Storm v2
'blackberry9800', // Torch
'webOS', // Palm Pre Experimental
'incognito', // Other iPhone browser
'webmate' // Other iPhone browser
);
$pattern = '/'.implode('|', $useragents).'/i';
return preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
}


register_sidebar( array(
'name' => 'フッター１',
'id' => 'footer_1',
'description' => '新規ウィジェットエリア',
'before_widget' => '',
'after_widget' => '',
) );







// オキニにいれた恋するアゲハ嬢

function fav_post_list() {

if($_SESSION['id'] != null){





  foreach($_SESSION['id'] as $session_data){


    query_posts('posts_per_page=1&p='.$session_data);


    if(have_posts()) {

        while(have_posts()) {

            the_post();


        echo '<li><a target="_self" title="';
		
			the_title();

		echo '" href="';

            the_permalink();

            echo '" />';

            the_post_thumbnail('thumbnail', array('class' => 'wpp-thumbnail wpp_cached_thumb wpp_featured'));

            echo '</a>';

		echo '<a class="wpp-post-title" target="_self" title="';

		the_title();

		echo '" href="';

		the_permalink();

	        echo '" />';

		the_title();

		 echo '[身長:';

		 echo  get_post_meta(get_the_ID(),'身長', true);

           	 echo 'cm]</a>';
				
	echo '</li>';

        }

    }

}

    wp_reset_query();


}


}


// オキニにいれた恋するアゲハ嬢(トップ表示用)

function fav_post_list_index() {

if($_SESSION['id'] != null){

$z = 0;

echo '<ul class="p_ts10 p_b20">';


  foreach($_SESSION['id'] as $session_data){

    query_posts('posts_per_page=1&p='.$session_data);


    if(have_posts()) {


        while(have_posts()) {

            the_post();


        echo '<li><a target="_self" title="';
		
			the_title();

		echo '" href="';

            the_permalink();

            echo '" />';

            the_post_thumbnail(array(34, 34), array('class' => 'wpp-thumbnail wpp_cached_thumb wpp_featured'));

            echo '</a>';



        }

    }
    
    $z++;
    
    if($z == 7){
    
    	break;
    
    }

}

echo '</ul>';        


	echo "<br />";

	echo "<hr class='clear' />";

    wp_reset_query();


}


if($_SESSION['user_id'] != NULL){



	//ログインシーケンス
	ini_set('include_path', '/home/bangkok-club/thai-kawaii-ageha.com/public_html/girls/wp-content/themes/ageha-g/pear/' . PATH_SEPARATOR);
	require_once("/home/bangkok-club/thai-kawaii-ageha.com/public_html/girls/wp-content/themes/ageha-g/function/init.php");
	require_once("/home/bangkok-club/thai-kawaii-ageha.com/public_html/girls/wp-content/themes/ageha-g/function/db_select.php");
	$db_access = db_access();




		//お気に入りデータの呼び出し
		$fav_data =	main_select($db_access,"*","user_fav_girls_tb","fav_girls_id",
		"WHERE fav_girls_del = 0 and fav_girls_attach_id = ".$_SESSION['user_id']." "," limit 7");



	echo '<ul class="p_ts10 p_b20">';


	$i = 0;
	//ユーザーがログインしている時の処理
	foreach($fav_data as $fav){


    	query_posts('posts_per_page=1&p='.$fav['fav_girls_post_id']);


	    if(have_posts()) {

	        while(have_posts()) {

            	the_post();


        	echo '<li><a target="_self" title="';
		
				the_title();

			echo '" href="';

            	the_permalink();

            	echo '" />';

            	 the_post_thumbnail(array(34, 34), array('class' => 'wpp-thumbnail wpp_cached_thumb wpp_featured'));

        	    echo '</a>';


        	}

    	}	
    	$i++;

    	
	
	}
	

echo '</ul>';        


	echo "<br />";

	echo "<hr class='clear' />";	
	

}




}



// オキニにいれた恋するアゲハ嬢(お問い合わせ用)

function fav_contact_list($db_access,$post_data) {


if($post_data['id'] != NULL){


	//データが一個指定の場合は一個だけ表示
	

	  	  query_posts('posts_per_page=1&p='.$post_data['id']);


    			if(have_posts()) {
    			
    	
    			
        			while(have_posts()) {

          		 	 	the_post();

						$image = the_post_thumbnail(array(85,85), array('class' => 'wpp-thumbnail wpp_cached_thumb wpp_featured'),true);
						

						$return_data .= '<span　class="fs9">' .$image . '</span>';

					}

		  	  }
   			 wp_reset_query();



}else{

	if($_SESSION['user_id'] != NULL){


	
	
		//会員ログインしていた場合お気に入りからデータを得る
		//require_once("/home/bangkok-club/thai-kawaii-ageha.com/public_html/girls/wp-content/themes/ageha-g/function/db_select.php");
		$db_access = db_access();


	
		//お気に入りデータの呼び出し
		$fav_data =	main_select($db_access,"*","user_fav_girls_tb","fav_girls_id",
		"WHERE fav_girls_del = 0 and fav_girls_attach_id = ".$_SESSION['user_id']." "," limit 10");
	
		if($fav_data != NULL){
		$i=0;
		foreach ($fav_data as $fav){
		

		if($i<=2){
	  	  query_posts('posts_per_page=1&p='.$fav['fav_girls_post_id']);


    			if(have_posts()) {


        			while(have_posts()) {


          		 	 	the_post();

						$image = the_post_thumbnail(array(85,85), array('class' => 'wpp-thumbnail wpp_cached_thumb wpp_featured'),true);

						$return_data .= '<span　class="fs9">' .$image . '</span>';


        			}

		  	    }
			}

		$i++;	
		
		}
		}
	
	
	}else{

		if($_SESSION['id'] != null){



		  $i=0;
		  foreach($_SESSION['id'] as $session_data){

		if($i<=2){
	  	  query_posts('posts_per_page=1&p='.$session_data);


    			if(have_posts()) {

        			while(have_posts()) {

          		 	 	the_post();

						$image = the_post_thumbnail(array(85,85), array('class' => 'wpp-thumbnail wpp_cached_thumb wpp_featured'),true);

						$return_data .= '<span　class="fs9">' .$image . '</span>';


        			}

		  	  }
			$i++;
			}

		}//foreach

   		 wp_reset_query();
   		 
		}
	}
}//post end	

return $return_data;

}
wpcf7_add_shortcode('fav_contact', 'fav_contact_list');





function my_form_tag_filter($tag){
if ( ! is_array( $tag ) )
return $tag;


//echo '呼び出され';

if($_SESSION['user_id'] != NULL){

//echo '会員IDあり呼び出され';


	//ログインシーケンス
	ini_set('include_path', '/home/bangkok-club/thai-kawaii-ageha.com/public_html/girls/wp-content/themes/ageha-g/pear/' . PATH_SEPARATOR);
	require_once("/home/bangkok-club/thai-kawaii-ageha.com/public_html/girls/wp-content/themes/ageha-g/function/init.php");
	require_once("/home/bangkok-club/thai-kawaii-ageha.com/public_html/girls/wp-content/themes/ageha-g/function/db_select.php");
	$db_access = db_access();

	


		//予約を入れた嬢がいるかどうかの呼び出し
			$now = date('Y-m-d');
			$girls_rev_data =	main_select($db_access,"*","user_girls_rev_tb","user_girls_rev_id",
			"WHERE user_girls_rev_attach_id = '".$_SESSION['user_id']."' AND user_girls_rev_date < '".$now."' and user_girls_rev_end = 0 "," desc");			



	
		//お気に入りデータの呼び出し
		$fav_data =	main_select($db_access,"*","user_fav_girls_tb","fav_girls_id",
		"WHERE fav_girls_del = 0 and fav_girls_attach_id = ".$_SESSION['user_id']." "," limit 3");


		//会員データ
		$user_data =	main_select($db_access,"user_tb.*,user_info_tb.*","user_tb 
		LEFT JOIN user_info_tb ON user_tb.user_id = user_info_tb.user_info_attach_id ","user_id",
		"WHERE user_del = 0 and user_id = ".$_SESSION['user_id']." "," limit 1");




if($girls_rev_data != NULL){




//予約データがある場合の判定
	$def_array = array(
	
	'your-number',
	'your-girl',
	'value',
	
	);
	
		//post_get値取得
		foreach($def_array as $key => $val){
		
			
			if(isset($_REQUEST[$val]) && $_REQUEST[$val] != ""){

					$post_data[$val] = trim($_REQUEST[$val]);

				
			}else{

					$post_data[$val] = "";

			}	
		
		}


	$name = $tag['name'];
	if($name == 'your-email'){

            $your_number = $user_data[0]['user_login_id'];
            $tag['values'] = (array) $your_number ;

	}


	$name = $tag['name'];
	if($name == 'your-tel'){

            $your_number = $user_data[0]['user_info_tel'];
            $tag['values'] = (array) $your_number ;

	}

	$name = $tag['name'];
	if($name == 'your-name'){

            $your_number = $user_data[0]['user_info_name'];
            $tag['values'] = (array) $your_number ;

	}


	$name = $tag['name'];
	if($name == 'user_id'){

            $user_id = $user_data[0]['user_info_attach_id'];
            $tag['values'] = (array) $user_id ;

	}

	$name = $tag['name'];
	if($name == 'user_girls_rev_date'){

            $user_girls_rev_date = $girls_rev_data[0]['user_girls_rev_date'];
            $tag['values'] = (array) $user_girls_rev_date ;

	}

	$name = $tag['name'];
	if($name == 'value'){

            $val = $_REQUEST['value'];
            $tag['values'] = (array) $val ;

	}







			if($_REQUEST['your-number'] != NULL ){

				$name = $tag['name'];
				if($name == 'your-number'){

    	        		$your_number = $post_data['your-number'];
	            		$tag['values'] = (array) $your_number ;

				}
			}
			if($_REQUEST['your-girl'] != NULL){

				$name = $tag['name'];
				if($name == 'your-girl'){

		            	$name_set = $post_data['your-girl'];
		        	    $tag['values'] = (array) $name_set ;

				}
			
			}
			
		




}else{

//予約データがない場合



///////もしひとつだけデータがあったらその子だけを予約するようにする
		$def_array = array(
	
		'name_set',
		'no',
		'id',
	
		);

		//post_get値取得
		foreach($def_array as $key => $val){
		
			
			if(isset($_REQUEST[$val]) && $_REQUEST[$val] != ""){

					$post_data[$val] = trim($_REQUEST[$val]);

				
			}else{

					$post_data[$val] = "";

			}	
		
		}


		if($post_data['id'] != NULL){
		
		
			$name = $tag['name'];
			if($name == 'your-number'){

            		$your_number = $post_data['no'];
            		$tag['values'] = (array) $your_number ;

			}

			$name = $tag['name'];
			if($name == 'your-girl'){

		            $name_set = $post_data['name_set'];
		            $tag['values'] = (array) $name_set ;

			}
		
		
		}else{
		///////もしお気に入り一人だけ予約を入れた場合のセット


		if(isset($fav_data[0])){

			$name = $tag['name'];
			if($name == 'your-number'){

            		$your_number = $fav_data[0]['fav_girls_num'];
            		$tag['values'] = (array) $your_number ;

			}

			$name = $tag['name'];
			if($name == 'your-girl'){

		            $name_set = $fav_data[0]['fav_girls_name'];
		            $tag['values'] = (array) $name_set ;

			}
		}

		if(isset($fav_data[1])){

			$name = $tag['name'];
			if($name == 'text-297'){

            		$your_number = $fav_data[1]['fav_girls_num'];
            		$tag['values'] = (array) $your_number ;

			}


			$name = $tag['name'];
			if($name == 'your-girl2'){

            		$name_set = $fav_data[1]['fav_girls_name'];
            		$tag['values'] = (array) $name_set ;

			}

	
		}


		if(isset($fav_data[2])){

			$name = $tag['name'];
			if($name == 'text-953'){

		            $your_number = $fav_data[2]['fav_girls_num'];
        		    $tag['values'] = (array) $your_number ;

			}


			$name = $tag['name'];
			if($name == 'your-girl3'){

        		    $name_set = $fav_data[2]['fav_girls_name'];
            		$tag['values'] = (array) $name_set ;

			}
		}


			$name = $tag['name'];
			if($name == 'your-email'){

            		$your_number = $user_data[0]['user_login_id'];
        		    $tag['values'] = (array) $your_number ;

			}


			$name = $tag['name'];
			if($name == 'your-tel'){

  		          $your_number = $user_data[0]['user_info_tel'];
		            $tag['values'] = (array) $your_number ;

			}


			$name = $tag['name'];
			if($name == 'your-name'){

   		         $your_number = $user_data[0]['user_info_name'];
   		         $tag['values'] = (array) $your_number ;

			}



	}



}///////もしひとつだけデータがあったらその子だけを予約するようにするここまで




}else{//log inしているかどうかのエルス判定
//ログインしてない場合


///////もしひとつだけデータがあったらその子だけを予約するようにする
		$def_array = array(
	
		'name_set',
		'no',
		'id',
	
		);

		//post_get値取得
		foreach($def_array as $key => $val){
		
			
			if(isset($_REQUEST[$val]) && $_REQUEST[$val] != ""){

					$post_data[$val] = trim($_REQUEST[$val]);

				
			}else{

					$post_data[$val] = "";

			}	
		
		}


		if($post_data['id'] != NULL){
		
		
			$name = $tag['name'];
			if($name == 'your-number'){

            		$your_number = $post_data['no'];
            		$tag['values'] = (array) $your_number ;

			}

			$name = $tag['name'];
			if($name == 'your-girl'){

		            $name_set = $post_data['name_set'];
		            $tag['values'] = (array) $name_set ;

			}
		
		
		}else{






	if(isset($_SESSION['no'][0])){

			$name = $tag['name'];
			if($name == 'your-number'){

            $your_number = $_SESSION['no'][0];
            $tag['values'] = (array) $your_number ;

	}
}
if(isset($_SESSION['name_set'][0])){

	$name = $tag['name'];
	if($name == 'your-girl'){

            $name_set = $_SESSION['name_set'][0];
            $tag['values'] = (array) $name_set ;

	}
}
if(isset($_SESSION['no'][1])){

	$name = $tag['name'];
	if($name == 'text-297'){

            $your_number = $_SESSION['no'][1];
            $tag['values'] = (array) $your_number ;

	}
}
if(isset($_SESSION['name_set'][1])){

	$name = $tag['name'];
	if($name == 'your-girl2'){

            $name_set = $_SESSION['name_set'][1];
            $tag['values'] = (array) $name_set ;

	}
}
if(isset($_SESSION['no'][2])){

	$name = $tag['name'];
	if($name == 'text-953'){

            $your_number = $_SESSION['no'][2];
            $tag['values'] = (array) $your_number ;

	}
}
if(isset($_SESSION['name_set'][2])){

	$name = $tag['name'];
	if($name == 'your-girl3'){

            $name_set = $_SESSION['name_set'][2];
            $tag['values'] = (array) $name_set ;

	}
}


}


}



return $tag;

}
add_filter('wpcf7_form_tag', 'my_form_tag_filter');



function add_user_id(){

	
	if($_SESSION['user_id'] != NULL){
	
		$user_id = $_SESSION['user_id'];
	
	}else{

		$user_id = NULL;

	}


    return $user_id;
}
wpcf7_add_shortcode("user_id","add_user_id");



