<?php
//============================================================================
//====以下、langset.php 内での設定 ===========================================
//============================================================================

//  output: $lang[*]['v']   言語名表示
//          $lang[*]['c']   サイト内での使用する言語コード名
//          $lang[*]['b']   ブラウザが発行する言語名

// europashe Sprache    
  $tmp['v'] = "English";   $tmp['c'] = "eng"; $tmp['b'] = "en";  $lang[]=$tmp;
  $tmp['v'] = "Deutsch";   $tmp['c'] = "deu"; $tmp['b'] = "de";  $lang[]=$tmp;
  $tmp['v'] = "Francias";  $tmp['c'] = "fra"; $tmp['b'] = "fr";  $lang[]=$tmp;

// asianische Sprache
  $tmp['v'] = "日本語";    $tmp['c'] = "jpn"; $tmp['b'] = "ja";  $lang[]=$tmp;
  $tmp['v'] = "中文(簡体)";$tmp['c'] = "chs"; $tmp['b'] = "zh";  $lang[]=$tmp;
  $tmp['v'] = "中文(繁体)";$tmp['c'] = "cht"; $tmp['b'] = "zh";  $lang[]=$tmp;
