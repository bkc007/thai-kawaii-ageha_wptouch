<?php

	require_once("Mail.php");
	require_once("Mail/mime.php");	




function regist_mail($db_access,$login_id,$user_check_pass,$user_info_name){



mb_language("japanese");
//言語指定
mb_internal_encoding("UTF8");	


//ヘッダ作成
$header['from'] = "info@thai-kawaii-ageha.com";
$header['to'] = $login_id;

//管理用アドレス
$header_2['to'] = "keichi.bless@gmail.com";
$header['subject'] = mb_encode_mimeheader("【恋するアゲハ嬢】会員登録ありがとうございます");

$body = NULL;

//echo $login_id;


$body .= "
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
AGEHA からのお知らせ
　　　　　　　　　　―3日間以内に本メールから登録を完了してください―
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
".$user_info_name." 様

この度は恋するアゲハ嬢 に「会員仮登録」をいただき、ありがとうございます。

***********************************************************************

以下のURLをクリックし、「本登録」を完了してください。（3日間のみ有効）

→ https://www.thai-kawaii-ageha.com/girls/regist_complete/?check=".$user_check_pass."
（※クリック後、会員登録が完了いたします）

***********************************************************************

※3日間以上経過すると、上記URLは無効となります。
　お手数ですが、再度ログインの上、認証メールを再送信してください。

………………………………………………………………………………………………
▼このメールにお心当たりがない場合

大変お手数ですがこのメールの中身を消さず、
「このメールに心当たりがない」旨をメールの最上部に記してご返信ください。


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

タイ・バンコクタニヤ AGEHA https://www.thai-kawaii-ageha.com/

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

";


//本文
$body = mb_convert_encoding($body,"JIS","UTF8");



//SMTP
$param = array("host"=>"sv1318.xserver.jp","port"=>25, "username"=>"info@thai-kawaii-ageha.com","password"=>"bangkok1000","auth"=>True);



$email =& Mail::factory("SMTP",$param);


//var_dump($sendadr);


//送信
$result = $email -> send($header['to'],$header,$body);
//管理用送信
//$result = $email -> send($header_2['to'],$header,$body);

//var_dump($result);


//エラー判定
if (PEAR::isError($result)){
echo $result -> getMessage();
}

}




function regist_complete_mail($db_access,$login_id,$user_first_name){




	mb_language("japanese");	
	
	
	mb_internal_encoding("UTF8");	



		//ヘッダ作成
		$header['from'] = "info@thai-kawaii-ageha.com";
		$header['to'] = $login_id;
		//管理用アドレス
		$header_2['to'] = "keiichi@moonbless.net";

		$header['subject'] = mb_encode_mimeheader("【恋するアゲハ嬢】本会員登録完了のお知らせ");

	
		$body = NULL;



		$body .= "
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
恋するアゲハ嬢 からのお知らせ
　　　　　　－会員本登録が完了しました－
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
".$user_first_name."様

www.thai-kawaii-ageha.comの会員本登録が完了しました。
このメールはご登録確認のため送信させて頂いております。

**********************************************************************
■登録メールアドレス
　".$login_id."

※登録内容を変更される場合は、下記URLよりお手続きください。
→ https://www.thai-kawaii-ageha.com/girls/login/
　（ログインが必要です）

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

タイ・バンコク AGEHA https://www.thai-kawaii-ageha.com/

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

";


//本文
$body = mb_convert_encoding($body,"JIS","UTF8");

//SMTP
$param = array("host"=>"sv1318.xserver.jp","port"=>25, "username"=>"info@thai-kawaii-ageha.com","password"=>"bangkok1000","auth"=>True);



$email =& Mail::factory("SMTP",$param);


//var_dump($sendadr);


	//送信
	$result = $email -> send($header['to'],$header,$body);
	//管理用送信
	//$result = $email -> send($header_2['to'],$header,$body);



	//エラー判定
	if (PEAR::isError($result)){
	echo $result -> getMessage();	
	}


}



function forget_mail($db_access,$login_id,$password){

echo 'send_mail';

	//言語指定
	mb_language("japanese");	


		//ヘッダ作成
		$header['from'] = "info@thai-kawaii-ageha.com";
		$header['to'] = $login_id;
		
		//管理用アドレス
		$header_2['to'] = "info@thai-kawaii-ageha.com";



			$header['subject'] = mb_encode_mimeheader("[恋するアゲハ嬢]パスワード再発行");		
		$body = NULL;
		//echo $login_id;
		//本文基本///////////////////////////////////////////
		
		$body .= "
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n
AGEHA からのお知らせ\n　
	    ―「thai-kawaii-ageha.com/girls/ パスワード」を再送いたします―\n
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n
".$login_id." 様\n
					
いつも「恋するアゲハ嬢」をご利用いただき、ありがとうございます。\n
					
下記、「thai-kawaii-ageha.com パスワード」をご案内いたします。\n

					
***********************************************************************\n
					
【thai-kawaii-ageha.com/girls/ パスワード】\n
".$password."\n
					

					　
恋するアゲハ嬢 ログインはこちらから\n
→ https://www.thai-kawaii-ageha.com/girls/login./\n
					
***********************************************************************\n
					
▼このメールにお心当たりがない場合\n\n
					
大変お手数ですがこのメールの中身を消さず、\n
「このメールに心当たりがない」旨をメールの最上部に記してご返信ください。\n
										
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

恋するアゲハ嬢 https://www.thai-kawaii-ageha.com/girls/

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━";

	
//本文
$body = mb_convert_encoding($body,"JIS","UTF8");

//SMTP
$param = array("host"=>"sv1318.xserver.jp","port"=>25, "username"=>"info@thai-kawaii-ageha.com","password"=>"bangkok1000","auth"=>True);



$email =& Mail::factory("SMTP",$param);

//var_dump($sendadr);

//送信
$result = $email -> send($header['to'],$header,$body);
//管理用送信
//$result = $email -> send($header_2['to'],$header,$body);

var_dump($result);


//エラー判定
if (PEAR::isError($result)){
echo $result -> getMessage();
}


}

			
?>