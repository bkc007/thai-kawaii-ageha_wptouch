<?php



	function profile_check($post_data){

		$post_data['error'] = NULL;


				if($post_data['re_user_info_mail'] == NULL || $post_data['user_info_mail'] == NULL){

					$post_data['mode'] = NULL;
					$post_data['error_mail'] .= "メールアドレスを入力してください";
					$post_data['error'] = 1;
				
				}else{



					
						if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $post_data['user_info_mail'])) {
		
							$post_data['mode'] = NULL;
							$post_data['error_mail'] .= "正しいメールアドレスを入力してください。";
							$post_data['error'] = 1;
								
						}
				}


		return $post_data;

	}


	function val_change_password($post_data,$data){

		$post_data['error'] = NULL;


		if(md5($post_data['now_password']) != $data[0]['user_pass']){
		
		
			$post_data['error'] ='入力内容が間違っています。';
			
		
			
		}
		
		if($post_data['re_password'] != $post_data['new_password']){
		
			$post_data['error'] = '入力されたパスワードが同じでありません。';
			
		}


		//入力チェック
		
		if($post_data['now_password'] == NULL or $post_data['new_password'] == NULL or $post_data['re_password'] == NULL){
		
			$post_data['error'] = '必要事項はすべてご入力ください。';
		
		}





		return $post_data;

	}



	function val_check_admin($post_data,$db_access){




		$post_data['error'] = NULL;
		

		

				if($post_data['admin_login_id'] == NULL || $post_data['admin_login_id'] == NULL){

					$post_data['mode'] = NULL;
					$post_data['error_admin_login_id'] .= "メールアドレスを入力してください";
					$post_data['error'] = 1;
				
				}else{



					
						if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $post_data['admin_login_id'])) {
		
							$post_data['mode'] = NULL;
							$post_data['error_admin_login_id'] .= "正しいメールアドレスを入力してください。";
							$post_data['error'] = 1;
								
						}else{


							//DBにコネクトして確認
							//このアドレスが登録されているかどうかをチェックする
							
							$admin_data = main_select($db_access,"admin_login_id","admin_tb","admin_id",
							"WHERE admin_login_id = '".$post_data['admin_login_id']."' AND admin_del = 0",$sort);
							
							
							if($admin_data != NULL){
								$post_data['mode'] = NULL;
								$post_data['error_admin_login_id'] = "このアドレスは既に登録されています";
								$post_data['error'] = 1;
							}


							
						}
					
				}
				
				


				if($post_data['admin_pass'] == NULL || $post_data['admin_pass'] == NULL || $post_data['re_admin_pass'] == NULL || $post_data['re_admin_pass'] == NULL){

					$post_data['mode'] = NULL;
					$post_data['error_admin_pass'] = "パスワードを入力してください";
					$post_data['error'] = 1;
				
				}else{
					
					//password Match check
					
					if($post_data['admin_pass'] != $post_data['re_admin_pass']){
					
						$post_data['mode'] = NULL;
						$post_data['error_admin_pass'] = "入力されたパスワードが同じではありません。";
						
					}
					
					
					//文字数カウント
					//if($post_data['password'])
					
					
				}

		



		return $post_data;




	}






	function val_check_member($db_access,$post_data){

		$post_data['error'] = NULL;
		
		
		echo "ヴァリデーションチェック";
		
	
		
				if($post_data['user_login_id'] == NULL || $post_data['user_login_id'] == NULL){

					$post_data['mode'] = NULL;
					$post_data['error_user_id'] .= "ログインIDが入力されていません";
					$post_data['error'] = 1;
				
				}else{



					
						if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $post_data['user_login_id'])) {
		
							$post_data['mode'] = NULL;
							$post_data['error_user_id'] .= "ログインIDは有効なメールアドレスを入力してください";
							$post_data['error'] = 1;
								
						}else{


							//DBにコネクトして確認
							//このアドレスが登録されているかどうかをチェックする
						
							$user = main_select($db_access,"*",'user_tb','user_id',
								"WHERE user_login_id = '".$post_data['user_login_id']."' AND user_del = 0 AND user_check = 1  ",'asc',$ini['debug']);	


							if($user != NULL){


							$post_data['mode'] = NULL;
							$post_data['error_user_id'] .= "このアドレスは既に登録されています";
							$post_data['error'] = 1;
							
							
							}


							
						}
					
				}
				
				


				/*if($post_data['password'] == NULL || $post_data['password'] == NULL || $post_data['re_password'] == NULL || $post_data['password'] == NULL){

					$post_data['mode'] = NULL;
					$post_data['error_password'] = "パスワードが入力されていません";
					$post_data['error'] = 1;
				
				}else{
					
					//password Match check
					
					if($post_data['password'] != $post_data['re_password']){
					
						$post_data['mode'] = NULL;
						$post_data['error_password'] = "入力されたパスワードが同じではありません";
						
					}
					
					
					//文字数カウント
					//if($post_data['password'])
					
					
				}*/
				
				

		
		
		return $post_data;

	
	
	}					
	
	
	function val_customer_update ($post_data){
		
		
						//メールアドレスのチェック
						
						if($post_data['customer_mail'] != NULL){
						
							if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $post_data['customer_mail'])) {
			
								$post_data['mode'] = NULL;
								$post_data['error_customer_mail'] .= "Please Entry Correct your E-mail";
								$post_data['error'] = 1;
									
							}		

						}else{
						
								$post_data['error_mail'] ="Please Entry E-mail";
								$post_data['error'] = 1;
								
						}
						
						//性別チェック
						
						if($post_data['customer_gender'] == NULL){
						
							$post_data['error_gender'] ="Please Select Gender";
							$post_data['error'] = 1;
							
						}


							/*if (!preg_match("/^([0-9])+([-])+$/", $post_data['customer_tel'])) {
			
								$post_data['mode'] = NULL;
								$post_data['error_customer_tel'] .= "Please Correct your Phone Number";
								$post_data['error'] = 1;
									
							}		*/					
						
		
		return $post_data;
	}
	


	function val_admin_regist ($post_data,$db_access){
		
		
								//メールアドレスのチェック
						
						if($post_data['admin_login_id'] != NULL){
						

						
							if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $post_data['customer_mail'])) {
			
								$post_data['mode'] = NULL;
								$post_data['error_admin_login_id'] .= "メールアドレスを正しく入力してください";
								$post_data['error'] = 1;
									
							}else{

								//登録の重複がないか確認処理
								$val = main_select($db_access,"count(*) as count ","admin_tb",$column," where admin_login_id = ".$post_data['admin_login_id']." " ,"asc");	
								
								if($val != NULL){

								$post_data['mode'] = NULL;
								$post_data['error_admin_login_id'] .= "メールアドレスを正しく入力してください";
								$post_data['error'] = 1;
																
							
								}				
								
								
								
							}

						}else{
						
								$post_data['error_mail'] ="メールアドレスを入力してください";
								$post_data['error'] = 1;
								
						}
		
		
		
				if($post_data['admin_pass'] == NULL || $post_data['re_admin_pass'] == NULL){

					$post_data['mode'] = NULL;
					$post_data['error_admin_pass'] = "パスワードが入力されていません";
					$post_data['error'] = 1;
				
				}else{
					
					//password Match check
					
					if($post_data['admin_pass'] != $post_data['re_admin_pass']){
					
						$post_data['mode'] = NULL;
						$post_data['error_re_admin_pass'] = "入力されたパスワードが同じではありません";
						
					}		
				}
		
		return $post_data;		
		
		}
	
	
	
	
	
		function val_client_regist ($post_data){
		
		
								//メールアドレスのチェック
						
						if($post_data['admin_info_mail'] != NULL){
						

						
							if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $post_data['admin_info_mail'])) {
			
								$post_data['mode'] = NULL;
								$post_data['error_admin_info_mail'] .= "メールアドレスを正しく入力してください";
								$post_data['error'] = 1;
									
							}
							

						}else{
						
								$post_data['error_admin_info_mail'] ="メールアドレスを入力してください";
								$post_data['error'] = 1;
								
						}
		
		
		
				if($post_data['admin_info_company_name'] == NULL ){

					$post_data['mode'] = NULL;
					$post_data['error_admin_info_company_name'] = "会社名が入力されていません。";
					$post_data['error'] = 1;
				
				}
				
				if($post_data['admin_info_name'] == NULL ){

					$post_data['mode'] = NULL;
					$post_data['error_admin_info_name'] = "担当者名が入力されていません。";
					$post_data['error'] = 1;
				
				}				
				
				if($post_data['admin_info_name_kana'] == NULL ){

					$post_data['mode'] = NULL;
					$post_data['error_admin_info_name_kana'] = "担当者名	カナが入力されていません。";
					$post_data['error'] = 1;
				
				}		

				if($post_data['company_title'] == NULL ){

					$post_data['mode'] = NULL;
					$post_data['error_company_title'] = "キャッチコピーは必ず入力してください";
					$post_data['error'] = 1;
				
				}			
				
				if($post_data['admin_info_address_number'] == NULL ){

					$post_data['mode'] = NULL;
					$post_data['error_admin_info_address_number'] = "郵便番号が入力されていません";
					$post_data['error'] = 1;
				
				}				
				

				if($post_data['admin_info_address'] == NULL ){

					$post_data['mode'] = NULL;
					$post_data['error_admin_info_address_number'] = "住所が入力されていません";
					$post_data['error'] = 1;
				
				}		
				

				if($post_data['admin_info_tel'] == NULL ){

					$post_data['mode'] = NULL;
					$post_data['error_admin_info_tel'] = "電話番号が入力されていません";
					$post_data['error'] = 1;
				
				}																								
				
				
				
		return $post_data;		
		
		}
		
		

	function val_data_regist ($db_access,$post_data){
		
		
		
				if($post_data['data_title'] == NULL ){

					$post_data['mode'] = NULL;
					$post_data['error_data_title'] = "タイトルが入力されてません";
					$post_data['error'] = 1;
				
				}
				

				if($post_data['data_detail'] == NULL ){

					$post_data['mode'] = NULL;
					$post_data['error_data_detail'] = "詳細が入力されてません";
					$post_data['error'] = 1;
				
				}			
				

				if($post_data['region_all'] == NULL ){


					if($post_data['region_code_name'] == NULL){
					$post_data['mode'] = NULL;
					$post_data['error_data_region'] = "対応エリアが選択されてません。";
					$post_data['error'] = 1;
					}
				
				}
				
				
				//カテゴリに重複があるかどうかのチェック	data_attach_admin_id data_catagory_id
				
				$data_check = main_select($db_access,"*","data_tb","data_id",
										"WHERE data_attach_admin_id = ".$post_data['data_attach_admin_id']." AND data_category_id = ".$post_data['data_category_id']." ",$sort);
				
				
				//var_dump($data_check);
				
				if($data_check != NULL){
					$post_data['mode'] = NULL;
					$post_data['error_data_category'] = "このカテゴリは既に登録されています";
					$post_data['error'] = 1;				
				
				}
				
				
		return $post_data;		
		
		}
		
		
		
		?>